import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./styles/common/main.scss";
import { BASE_API_URL } from "./settings/app.settings";
import ApiService from "./services/api.service";

Vue.config.productionTip = false;

ApiService.init(BASE_API_URL);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
