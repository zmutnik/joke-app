import { JOKE_API_SETTINGS } from "../../settings/app.settings";
import ApiService from "../../services/api.service";
import { A_FETCH_JOKES } from "../action.types";
import {
  M_SET_JOKES,
  M_UPDATE_JOKE_SEARCH,
  M_UPDATE_JOKE_FILTER,
  M_SET_JOKE_CATEGORIES,
} from "../mutation.types";
import { GET_JOKES, GET_JOKE_CATEGORIES } from "../getter.types";

const state = {
  jokes: [],
  jokeCategories: [],
  jokeSearch: "",
  jokeFilter: "All",
};

const getters = {
  [GET_JOKES]: (state) => {
    let tempJokes;

    if (state.jokeSearch !== "") {
      tempJokes = state.jokes.filter((item) => {
        if (item.joke) {
          return item.joke
            .toLowerCase()
            .includes(state.jokeSearch.toLowerCase());
        } else if (item.setup && item.delivery) {
          return (
            item.setup.toLowerCase().includes(state.jokeSearch.toLowerCase()) ||
            item.delivery.toLowerCase().includes(state.jokeSearch.toLowerCase())
          );
        }
      });
    } else {
      tempJokes = state.jokes;
    }

    if (state.jokeCategories.includes(state.jokeFilter)) {
      tempJokes = tempJokes.filter(
        (item) => item.category === state.jokeFilter
      );
    }

    return tempJokes;
  },
  [GET_JOKE_CATEGORIES](state) {
    return state.jokeCategories;
  },
};

const actions = {
  async [A_FETCH_JOKES]({ commit }) {
    try {
      const res1 = ApiService.get("joke/Any", JOKE_API_SETTINGS);
      const res2 = ApiService.get("joke/Any", JOKE_API_SETTINGS);
      const res3 = ApiService.get("joke/Any", JOKE_API_SETTINGS);

      await Promise.all([res1, res2, res3]).then((values) => {
        const jokes = values
          .map((res) => {
            return res.data.jokes;
          })
          .flat();

        const uniqueJokes = [];
        const jokeCategories = new Set();

        jokes.forEach((item) => {
          if (!uniqueJokes.find((joke) => joke.id === item.id)) {
            uniqueJokes.push(item);
            jokeCategories.add(item.category);
          }
        });

        commit(M_SET_JOKES, uniqueJokes);
        commit(M_SET_JOKE_CATEGORIES, [...jokeCategories]);
      });
    } catch (error) {
      const isKnownError =
        error.response && error.response.data && error.response.data.message;

      commit(
        "general/M_ADD_ERROR",
        isKnownError ? error.response.data.message : "Something went wrong",
        {
          root: true,
        }
      );
    }
  },
};

const mutations = {
  [M_SET_JOKES](state, jokes) {
    state.jokes = jokes;
  },
  [M_SET_JOKE_CATEGORIES](state, jokeCategories) {
    state.jokeCategories = jokeCategories;
  },
  [M_UPDATE_JOKE_SEARCH](state, searchStr) {
    state.jokeSearch = searchStr;
  },
  [M_UPDATE_JOKE_FILTER](state, jokeFilter) {
    state.jokeFilter = jokeFilter;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
