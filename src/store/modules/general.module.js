import { GET_PAGE_LOADER_STATUS, GET_ERRORS } from "../getter.types";
import {
  M_ADD_ERROR,
  M_REMOVE_ERROR,
  M_CLEAR_ERRORS,
  M_UPDATE_PAGE_LOADER,
} from "../mutation.types";

const state = {
  isLoading: false,
  errors: [],
};

const getters = {
  [GET_PAGE_LOADER_STATUS]: (state) => state.isLoading,
  [GET_ERRORS]: (state) => state.errors,
};

const actions = {};

const mutations = {
  [M_UPDATE_PAGE_LOADER](state, isLoading) {
    state.isLoading = isLoading;
  },
  [M_ADD_ERROR](state, errorMessage) {
    state.errors.push(errorMessage);
  },
  [M_REMOVE_ERROR](state, index) {
    state.errors.splice(index, 1);
  },
  [M_CLEAR_ERRORS](state) {
    state.errors = [];
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
