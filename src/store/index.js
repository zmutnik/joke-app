import Vue from "vue";
import Vuex from "vuex";

import jokes from "./modules/jokes.module";
import general from "./modules/general.module";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    jokes,
    general,
  },
});
