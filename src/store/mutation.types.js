export const M_SET_JOKES = "M_SET_JOKES";
export const M_SET_JOKE_CATEGORIES = "M_SET_JOKE_CATEGORIES";
export const M_UPDATE_JOKE_SEARCH = "M_UPDATE_JOKE_SEARCH";
export const M_UPDATE_JOKE_FILTER = "M_UPDATE_JOKE_FILTER";
export const M_UPDATE_PAGE_LOADER = "M_UPDATE_PAGE_LOADER";
export const M_ADD_ERROR = "M_ADD_ERROR";
export const M_REMOVE_ERROR = "M_REMOVE_ERROR";
export const M_CLEAR_ERRORS = "M_CLEAR_ERRORS";
