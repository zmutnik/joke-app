import axios from "axios";

const ApiService = {
  init(baseURL, headers = {}) {
    axios.defaults.baseURL = baseURL;
    axios.defaults.headers.common = headers;
  },

  get(resource, params = {}) {
    return axios.get(resource, params);
  },

  post(resource, data) {
    return axios.post(resource, data);
  },

  put(resource, data) {
    return axios.put(resource, data);
  },

  delete(resource) {
    return axios.delete(resource);
  },

  customRequest(data) {
    return axios(data);
  },
};

export default ApiService;
