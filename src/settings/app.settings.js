export const BASE_API_URL = "https://v2.jokeapi.dev/";
export const JOKE_DELIVERY_DELAY = 2000;
export const RANDOM_JOKE_UPDATE_DELAY = 30000;

export const JOKE_API_SETTINGS = {
  params: {
    format: "json",
    amount: 10,
    blacklistFlags: "nsfw,racist,sexist,explicit",
  },
};
